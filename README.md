##### build/start all containers
```
# check loki (promtail), prometheus and tempo config
#
$ docker-compose build
$ docker-compose up -d
```

##### sending request to spring boot
```
# create logs and traces
#
$ curl http://localhost:8080/api/users/1
```

##### checking grafana
```
# explore loki, prometheus and tempo
# admin/admin
#
$ http://localhost:3000
```

##### grafana: import dashboard for Spring Boot 2
```
https://grafana.com/grafana/dashboards/10280
```

##### start/stop/restart/remove containers
```
$ docker-compose start
$ docker-compose stop
$ docker-compose restart
$ docker-compose down
```

##### remove docker volume & network
```
$ docker volume ls
$ docker volume rm {volume_id}

$ docker network ls
$ docker network rm {network_id}
```

##### telemetry overview
![Scheme](images/telemetry.png)